package com.miramicodigo.intents

import android.Manifest
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import android.content.Intent
import android.app.Activity
import android.app.SearchManager
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.view.View

class MainActivity : AppCompatActivity() , View.OnClickListener {
    val DEVUELVE_DATOS = 2
    val PERMISO_LLAMADA = 123

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



    }

    override fun onClick(view: View?) {

    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == DEVUELVE_DATOS) {
            if (resultCode == Activity.RESULT_OK) {
                val resultado = data.getStringExtra("respuesta").toString()
                Toast.makeText(this, "RESPUESTA: $resultado", Toast.LENGTH_LONG).show()
            } else {
                Toast.makeText(this, "Se cancelo la respuesta", Toast.LENGTH_LONG).show()
            }
        }
    }

    fun llamar() {
        val permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(Manifest.permission.CALL_PHONE), PERMISO_LLAMADA)
        } else {

        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISO_LLAMADA -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                llamar()
            } else {
                Toast.makeText(this, "PERMISO DENEGADO", Toast.LENGTH_SHORT).show()
            }
        }
    }



}
