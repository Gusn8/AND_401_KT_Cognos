package com.miramicodigo.sqlite.db

import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import com.miramicodigo.sqlite.Constants

class DatabaseAdapter(context: Context) {

    var db: SQLiteDatabase? = null

    init {



    }

    fun abrir() {

    }

    fun cerrar() {

    }

    fun adicionarPersona(nombre: String, telefono: Long, correo: String, genero: String): Long {

        return 0
    }

    fun actualizarPersona(id: Long, nombre: String, telefono: Long,
                          correo: String, genero: String): Int {


        return 0
    }

    fun eliminarPersona(id: Long): Boolean {

        return false
    }

    fun obtenerPersona(id: Long): Cursor {

        return null!!
    }

    fun obtenerTodasPersonas(): Cursor {

        return null!!
    }

    private class PersonasDatabaseHelper(context: Context) : SQLiteOpenHelper(context, "dbpersonas.db", null, 1) {

        override fun onCreate(db: SQLiteDatabase) {


        }

        override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
            db.execSQL("DROP TABLE IF EXISTS ${Constants.TABLA_PERSONA}")
            onCreate(db)
        }
    }
}
