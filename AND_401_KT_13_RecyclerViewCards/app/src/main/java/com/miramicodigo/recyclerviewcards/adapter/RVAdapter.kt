package com.miramicodigo.recyclerviewcards.adapter

import android.content.Intent
import android.widget.TextView
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import com.miramicodigo.recyclerviewcards.ui.DetalleActivity
import com.miramicodigo.recyclerviewcards.R
import java.util.*

internal class RVAdapter(activity: Activity, private val items: ArrayList<Objects>) : RecyclerView.Adapter<RVAdapter.ViewHolder>() {

    private val context: Context

    init {
        this.context = activity
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
                .inflate(R.layout.card_item_grid, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tf_black = Typeface.createFromAsset(context.assets, "fonts/roboto_black.ttf")
        val tf_thin = Typeface.createFromAsset(context.assets, "fonts/roboto_thin.ttf")


    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {


        init {

        }
    }
}
